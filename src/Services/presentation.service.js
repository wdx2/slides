import PEngineer from './decks/PEngineer';
import PWATalk from './decks/PWATalk';

const staticPresentations = [
    PEngineer,
    PWATalk,
];

export default new (class PresentationService {
    getPresentations() {
        return Promise.resolve(staticPresentations);
    }
})();
