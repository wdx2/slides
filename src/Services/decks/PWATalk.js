export default {
    id: 'static-1',
    title: 'Web as a First Class Citizen',
    description: '',
    createdAt: 1579750105432,
    slides: [{
        md: "# **Web as a First Class Citizen**\n*Google Developers Group*",
        classes: ['dark', 'v-center', 'h-center']
    }, {
        md: "# 10000 Foot\n- Benefits of native apps\n - Benefits of mobile web apps\n - What is a Progressive Web App?\n - Criteria for PWA\n - OS support for PWA's\n - Possibilities of PWA\n - Examples of PWA's"
    }, {
        md: "# Benefits of Native Applications\n## **Engagement**\n  - Users spend 18.5x more time on a native app than on the equivalent mobile website <sup>1</sup>"
    }, {
        md: "# Possible Reasons\n - Easily launched from the homescreen\n - Use of push notifications to keep users engaged\n - More money spent to produce a premium experience"
    }, {
        md: "# Benefits of Mobile Optimized Website\n## **Acquiring**\n - ~ one in four users visiting an app store page will install the app<sup>1</sup>\n - Top 1000 mobile websites receive 270% more monthly visitors than the top 1000 apps<sup>1</sup>\n - Require very little data and no installation to acquire users"
    }, {
        md: "# Summarize\n\n> \"Conventional web sites let you reach users, while apps let you deeply engage users. This is the heart of the tradeoff between mobile web vs apps.\"",
        classes: ['h-center']
    }, {
        md: "# How do we bridge the best of both worlds?",
        classes: ['h-center', 'v-center', 'dark']
    }, {
        md: "# What is a Progressive Web App?\n - Web App: Just a web app we all know and love, with all the features of the web\n - Progressive: The progressive enhancement of the application\n   - This is akin to the pattern of feature flags\n   - As application finds API's which are available, it 'turns on' more features of the application"
    }, {
        md: "# Traits of a Progressive Web App\nGoogle<sup>2</sup>",
        classes: ['h-center']
    }, {
        md: "# Progressive\n - Works for all users regardless of browser\n - Uses principle of progressive enhancement"
    }, {
        md: "# Responsive\n - Approaches display fragmentation such that the app fits and effectively utilizes any form factor"
    }, {
        md: "# Connectivity Independent\n - PWA should continue to function no matter what connection the user is on\n - Can be accomplished using service workers to cache or store-and-forward inbound and outbound data"
    }, {
        md: "# Native App-like\n - Feels like a native app on the given OS and within the application\n - In app nativity can be accomplished by using the App Shell model that separates app functionality from app context"
    }, {
        md: "# Fresh\n - Apps should update in the background when users are accessing the application\n - Provides a Just-In-Time means of updating app functionality"
    }, {
        md: "# Safe\n - Platform enforces that all Progressive Web Apps must be served via SSL (https) to prevent snooping and tampering"
    }, {
        md: "# Discoverable\n - Use existing SEO strategies to optimize website discoverability\n - Manifest and Service Worker scope allow search engines to index the site as an app instead of just a website"
    }, {
        md: "# Re-engagement\n  - Bring users back to the app with features like push notifications"
    }, {
        md: "# Installable\n - Apps can be added to the homescreen without the middle step of an app store"
    }, {
        md: "# Linkable\n - Deep Linking\n - Share specific views of an app with a url and without the need for installation"
    }, {
        md: "*take a deep breath*\n# Any Questions?",
        classes: ['h-center', 'v-center', 'dark']
    }, {
        md: "# Android Install Process",
        classes: ['h-center', 'v-center', 'dark']
    }, {
        md: "# Other OS Support",
        classes: ['h-center', 'v-center', 'dark']
    }, {
        md: "# Chrome OS / Samsung Dex\n - Deck Icon\n - Stand-alone Window"
    }, {
        md: "# Windows 10\n - Announcement of App store listings for PWA's\n - Special Bing listing indicating it as an application complete with icon and manifest information"
    }, {
        md: "# Apple\n - Service workers are in \"Preview\" status on webkit<sup>3</sup>\n - Indicating OS level support in upcoming versions of iOS and MacOS"
    }, {
        md: "# Posibilities\n - What features would you expect from an application platform?"
    }, {
        md: "# Finally some real examples!",
        classes: ['h-center', 'v-center', 'dark']
    }, {
        md: "# Strengths of a PWA\n - Improved approach to fragmentation\n  - Browser abstracts away hardware differences\n  - Progressive enhancement to address api availability\n  - Responsive design to approach display sizing\n  - Browser can stay up-to-date independent of manufacturers or the OS"
    }, {
        md: "# Closing\n> A PWA is truly a write once and use anywhere application. This cuts cost, normalizes codebases, improves UX and makes our jobs easier.",
        classes: ['h-center', 'v-center', 'dark']
    }, {
        md: "# Annotations\n<sup>1</sup> https://medium.com/@owencm/the-surprising-tradeoff-at-the-center-of-question-whether-to-build-an-native-or-web-app-d2ad00c40fb2 <br/><sup>2</sup> https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/ \n <br/><sup>3</sup> https://webkit.org/blog/8090/workers-at-your-service/"
    }],
};