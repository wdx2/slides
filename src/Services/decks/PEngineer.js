export default {
    id: 'static-0',
    title: 'Javascript Principle Engineer',
    description: 'Lets talk about the opportunities and challenges for the Javascript principle at WWT Application Services',
    createdAt: 1579750105432,
    slides: [
        {
            md: [
                "# **Javascript Principle Engineer**",
                "*Devin Kiser*"
            ]
        },
        {
            md: [
                "# JS Ecosystem",
                " - Web Platform",
                " - Desktop/Server Runtime",
                " - Overlap other languages and principles",
            ]
        },
        {
            md: [
                "# **Good News - Opportunities**",
                "",
                "# **Bad News - Challenges**",
            ]
        },
        {
            md: [
                "*Good News - Diversify App Development*",
                "# Progressive Web Apps",
                " - Websites that can be installed",
                " - Supported on nearly every platform"
            ]
        },
        {
            md: [
                "# Examples",
                " - This app right here! [wdx2.gitlab.io/slides](https://wdx2.gitlab.io/slides)",
                " - [app.starbucks.com](https://app.starbucks.com)",
                " - [open.spotify.com](https://open.spotify.com)",
                " - [watch.lolesports.com](https://watch.lolesports.com)",
                " - Most google services (photos, maps, most recently drive)",
                "",
                "*[https://developers.google.com/web/showcase](https://developers.google.com/web/showcase)*"
            ]
        },
        {
            md: [
                "# Where's the opportunity!",
                " - Lower barrier to entry than other app platforms",
                "    - [alibaba.com](https://alibaba.com) (76% increase in conversions)",
                "# ",
                " - Provide more value at a lower cost",
                "    - Single codebase to maintain",
                "    - Get a free website out of the deal",
                "# ",
                " - Could be a fairly unique service to provide",
                "    - Adoption is just beginning"
            ]
        },
        {
            md: [
                "*Good News - Collaboration with JS*",
                "# So Much Collaboration",
            ]
        },
        {
            md: [
                "# Inter-principle Collaboration",
                " - Machine Learning",
                "    - TensorflowJS (execute, train, create)",
                "# ",
                " - Blockchain",
                "    - WebAssembly runtime for smart-contracts",
                "# ",
                " - .NET",
                "    - C# applications in the browser with Blazor and WebAssembly",
                "# ",
                " - Most of the others",
                "    - iOS",
                "    - Android",
                "    - Mobile Testing",
                "    - Accessibility",
            ]
        },
        {
            md: [
                "# Inter-disciplinary Collaboration",
                " - UX/Frontend-Engineers",
                " - QA - Automated Testing",
            ]
        },
        {
            md: [
                "# Inter-organization Collaboration",
                " - WWT IT Department does a large number of web-apps",
                " - Some infrastructure/backends in NodeJS",
                " - How do we get other departments involved as we build a community around JS?",
            ]
        },
        {
            md: [
                "*Pause*",
                "# Some Bad News - Challenges", 
            ]
        },
        {
            md: [
                "*Bad News - Security*",
                "# Securing Dependency Management",
                " - Not unique to JS",
                " - In Summary: We depend on code that we don't own or control",
                "# ",
                " - Credential Security",
                "    - July 12, 2018 - es-lint-scope",
                "    - module hijacked, update pushed",
                "    - Locking versions",
                "# ",
                " - Transitive dependencies",
                "    - Oct 5, 2018 - event-stream",
                "    - Bad actor had merge request accepted, new dependency",
                "    - Dependency of the new dependency updated to steal bitcoin",
                "# ",
                " - Exacerbated by CI/CD",
            ]
        },
        {
            md: [
                "*Bad News - Security*",
                "# Solutions",
                " - Maintain an App Services hosted registry that proxies to public registry(s)",
                "    - Existing Artifactory",
                " - Communicate, Train and Share Knowledge",
            ]
        },
        {
            md: [
                "*Bad News - Knowledge Sharing*",
                "# Knowledge Sharing",
                " - How?",
                " - Who checks the boxes to ensure a project is off to the right start?",
                "    - Needs something like the Agile Principles",
            ]
        },
        {
            md: [
                "*Bad News - JS Ecosystem is Huge*",
                "# Variety of Runtime Environments",
                " - Browsers: IE, Safari, Chrome, Firefox",
                " - Standalone Runtimes: NodeJS, ioJS",
                " - Embedded Systems",
            ]
        },
        {
            md: [
                "*Bad News - JS Ecosystem is Huge*",
                "# Large number of contributers",
                " - Overall the most used programming language of 2019",
                " - 2017 registry.npmjs.org was >2x the size of the next largest registry",
                "    - Java - Apache Maven",
                " - Intimidating",
                " - Too many choices, analysis paralysis",
            ]
        },
        {
            md: [
                "*Bad News - JS Ecosystem is Huge*",
                "# Solution For AS (and WWT)",
                " - Community",
                "    - No one could possibly always have the correct answer",
                "    - Need collaboration and communication across JS teams",
            ]
        },
        {
            md: [
                "# Conclusion",
                " - Excited",
                "    - Principle Engineer roles",
                "    - Organization being proactive",
                "    - Collaboration across the company",
            ]
        },
    ],
}