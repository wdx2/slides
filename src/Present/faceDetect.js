export default {
    start(videoSrc, cb) {
        // if (window.FaceDetector) {
        //     const detector = new FaceDetector({ maxDetectedFaces: 1, fastMode: true });
        //     this.runDetect(detector, cb);
        // } else {
            getFaceApiDetector().then(detector => {
                this.runDetect(detector, cb);
            });
        // }

        this.video = videoSrc;
    },
    stop() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
    },
    runDetect(detector, cb) {
        this.timeout = setTimeout(() => {
            detector.detect(this.video)
                .then(faces => {
                    if (faces) {
                        cb(faces[0]);
                    }

                    this.runDetect(detector, cb);
                }).catch(e => {
                    console.log('Failed to detect faces', e);
                    return getFaceApiDetector().then(faceApiDetector => this.runDetect(faceApiDetector, cb));
                });
        }, 2000);
    },
}

function getFaceApiDetector() {
    return faceapi.nets.ssdMobilenetv1.loadFromUri('./face-api').then(() => ({
        detect: (video) => faceapi.detectSingleFace(video).then(results => {
            if (!results) return;
            const box = results.box;
            return [{
                x: box.left,
                y: box.top,
                height: box.height,
                width: box.width,
            }];
        }),
    }));

}