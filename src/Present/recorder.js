let recordedChunks = [];
let mediaRecorder;

export default {
    start() {
        recordedChunks = [];
        navigator.mediaDevices.getDisplayMedia()
            .then(stream => {
                const options = { mimeType: "video/webm; codecs=vp9" };
                mediaRecorder = new MediaRecorder(stream, options);
                mediaRecorder.ondataavailable = (e) => {
                    if (e.data.size > 0) {
                        recordedChunks.push(e.data);
                    } else {
                        console.log('Nothing Recorded');
                    }
                };
                mediaRecorder.start();
            });
    },
    stop() {
        if (mediaRecorder) {
            mediaRecorder.stop();
            mediaRecorder.stream.getTracks().forEach(t => t.stop());
        }
    },
    download() {
        var blob = new Blob(recordedChunks, {
            type: "video/webm"
        });
        var url = URL.createObjectURL(blob);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        a.href = url;
        a.download = "recording.webm";
        a.click();
        window.URL.revokeObjectURL(url);
    }
}