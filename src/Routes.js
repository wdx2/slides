import Presentations from './Presentations/Presentations';
import Present from './Present/Present';
import FaceTracking from './FaceTracking/FaceTracking';

export default [
    { path: '/', component: Presentations },
    { path: '/present/:id', component: Present },
    { path: '/faceTrack', component: FaceTracking },
];
